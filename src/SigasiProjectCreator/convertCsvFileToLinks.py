#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    :copyright: (c) 2008-2017 Sigasi
    :license: BSD, see LICENSE for more details.
"""
import os

from SigasiProjectCreator import SigasiProjectCreator
from ArgsAndFileParser import ArgsAndFileParser
import CsvParser
import VhdlVersion


def get_file_name(entry):
    (folder, filename) = os.path.split(os.path.abspath(entry))
    return filename


def main():
    parser = ArgsAndFileParser(CsvParser.usage)
    (project_name, _, destination, entries) = parser.parse_args_and_file(CsvParser.parse_file)

    creator = SigasiProjectCreator(project_name, VhdlVersion.TWENTY_O_EIGHT)

    for path, library in entries.items():
        file_name = get_file_name(path)
        link_type = os.path.isdir(path)
        creator.add_link(file_name, os.path.abspath(path), link_type)
        creator.add_mapping(file_name, library)
        
    creator.add_unisim(os.environ.get('XILINX_VIVADO')+'/data/vhdl/src/unisims/')
    creator.add_xpm(os.environ.get('XILINX_VIVADO')+'/data/ip/xpm/')
    creator.add_uvvm(os.path.abspath('../../simulation/UVVM/'))

    creator.write(destination)


if __name__ == '__main__':
    main()
